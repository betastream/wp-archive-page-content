<?php 

namespace Page_IO\Archive_Page;

/*
*	adds the link to the archive page under the associated post type in the admin menu
*/
function admin_menu(){
	global $submenu;
	//ddbug( $submenu['edit.php?post_type=test'] );

	$post_types = get_post_types( ['has_archive' => TRUE] );
	
	foreach( $post_types as $post_type ){
		$index = 'edit.php?post_type='.$post_type;
		$query = query_page( $post_type );

		$post_id = $query->have_posts() ? $query->posts[0]->ID : create_page( $post_type );
		$link = sprintf( 'post.php?post=%d&action=edit', $post_id );

		$submenu[$index][6] = [
			'Archive Page Content',
			'edit_posts',
			admin_url( $link )
		];
	}
}
add_action( 'admin_menu', __NAMESPACE__.'\admin_menu', 100 );