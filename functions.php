<?php 

namespace Page_IO\Archive_Page;

/*
*
*	@param string
*	@return int
*/
function create_page( $post_type ){
	$post_id = wp_insert_post( [
		'post_status' => 'publish',
		'post_type' => 'archive-content'
	], '' );

	update_post_meta( $post_id, 'post_type', $post_type );
	
	return $post_id;
}

/*
*
*	@param string
*	@return WP_Query
*/
function query_page( $post_type ){
	return new \WP_Query( [
		'meta_query' => [
			[
				'key' => 'post_type',
				'value' => $post_type
			]
		],
		'post_type' => 'archive-content',
		'posts_per_page' => 1
	] );
}