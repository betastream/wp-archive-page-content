<?php 

namespace Page_IO\Archive_Page;

require __DIR__.'/functions.php';

if( is_admin() )
	require __DIR__.'/admin.php';

/*
*	register the archive-content post type
*/
function init(){
	register_post_type( 'archive-content', [
		//'exclude_from_search' => TRUE,
		'has_archive' => FALSE,
		'label' => 'Archive Content',
		'public' => TRUE,
		'rewrite' => [
			'slug' => 'archive-content'
		],
		'show_in_menu' => FALSE
	] );
}
add_action( 'init', __NAMESPACE__.'\init' );

/*
*	makes the query for archive post and avaialble in template as $archive_page
*	@param array
*	@param WP_Query
*	@return array
*/
function posts_results( $posts, $wp_query ){
	if( $wp_query->is_main_query() && $wp_query->is_archive() ){
		$archive_page = query_page( $wp_query->query_vars['post_type'] );

		$wp_query->set( 'archive_page', $archive_page );
	}

	return $posts;
}
add_filter( 'posts_results', __NAMESPACE__.'\posts_results', 10, 2 );

/*
*
*	@param string
*	@param WP_Post
*	@param bool
*	@param bool
*	@return string
*/
function post_type_link( $post_link, $post, $leavename, $sample ){
	if( $post->post_type == 'archive-content' ){
		$post_type = get_post_meta( $post->ID, 'post_type', TRUE );
		$post_link = get_post_type_archive_link( $post_type );
	}

	return $post_link;
}
add_filter( 'post_type_link', __NAMESPACE__.'\post_type_link', 10, 4 );