<?php
/*
Plugin Name: 	Page.IO Archive Page
Plugin URI: 
Description:	Adds post content for achive pages
Author:			
Version:		0.2.1
Author URI: 

This file must be parsable by php 5.2
*/

register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );

//
register_activation_hook( __FILE__, '\sjr\add_capabilities' );

require __DIR__.'/index.php';